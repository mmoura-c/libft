/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoura-c <student.42sp.org.br>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/02 14:09:48 by mmoura-c          #+#    #+#             */
/*   Updated: 2021/06/23 03:48:40 by mmoura-c         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t n)
{
	unsigned char	*dst_;
	unsigned char	*src_;

	dst_ = (unsigned char *)dst;
	src_ = (unsigned char *)src;
	if (dst_ < src_)
		return (ft_memcpy(dst, src, n));
	else
		while (n-- && dst != src)
			dst_[n] = src_[n];
	return (dst);
}
