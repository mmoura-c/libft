/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoura-c <student.42sp.org.br>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/02 14:19:20 by mmoura-c          #+#    #+#             */
/*   Updated: 2021/06/23 07:33:39 by mmoura-c         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s)
{
	size_t	s_len;
	char	*ptr;

	s_len = ft_strlen(s);
	ptr = malloc(sizeof(char) * (s_len + 1));
	if (!ptr)
		return (0);
	s_len = 0;
	while (s[s_len])
	{
		ptr[s_len] = s[s_len];
		s_len++;
	}
	ptr[s_len] = '\0';
	return (ptr);
}
