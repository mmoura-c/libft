/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoura-c <student.42sp.org.br>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/02 07:50:08 by mmoura-c          #+#    #+#             */
/*   Updated: 2021/06/22 22:13:06 by mmoura-c         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned char	s1_;
	unsigned char	s2_;

	while (n-- > 0)
	{
		s1_ = *(unsigned char *)s1++;
		s2_ = *(unsigned char *)s2++;
		if (s1_ != s2_)
			return (s1_ - s2_);
		if (s1_ == '\0')
			return (0);
	}
	return (0);
}
