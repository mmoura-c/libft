/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoura-c <student.42sp.org.br>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/22 23:38:01 by mmoura-c          #+#    #+#             */
/*   Updated: 2021/06/23 00:05:43 by mmoura-c         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned char		*dst_;
	const unsigned char	*src_;

	if (!dst && !src)
		return (0);
	dst_ = (unsigned char *)dst;
	src_ = (const unsigned char *)src;
	while (n-- && dst != src)
	{
		*dst_++ = *src_++;
	}
	return (dst);
}
