/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoura-c <student.42sp.org.br>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/30 18:12:35 by mmoura-c          #+#    #+#             */
/*   Updated: 2021/06/30 18:57:53 by mmoura-c         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void*))
{
	t_list	*aux;
	t_list	*auxnext;

	aux = *lst;
	auxnext = NULL;
	while (aux)
	{
		auxnext = aux->next;
		del(aux->content);
		free(aux);
		aux = auxnext;
	}
	*lst = NULL;
}
