/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoura-c <student.42sp.org.br>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/02 14:12:32 by mmoura-c          #+#    #+#             */
/*   Updated: 2021/06/23 07:00:19 by mmoura-c         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char	*src_;
	unsigned char	*dst_;

	src_ = (unsigned char *)src;
	dst_ = (unsigned char *)dst;
	while (n-- > 0)
	{
		*dst_++ = *src_;
		if (*src_ == (unsigned char)c)
			return (dst_);
		src_++;
	}
	return (0);
}
