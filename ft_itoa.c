/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoura-c <student.42sp.org.br>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/23 08:44:02 by mmoura-c          #+#    #+#             */
/*   Updated: 2021/06/23 12:32:12 by mmoura-c         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_count_digit(int n)
{
	if (!(n / 10))
		return (1);
	else
		return (ft_count_digit(n / 10) + 1);
}

char	*ft_itoa(int n)
{
	char			*str;
	unsigned int	nbr;
	size_t			len;

	len = ft_count_digit(n);
	if (n < 0)
	{
		nbr = -(unsigned int)n;
		len++;
	}
	else
		nbr = (unsigned int)n;
	str = malloc((len + 1) * sizeof(*str));
	if (!str)
		return (0);
	str[len] = '\0';
	while (len--)
	{
		str[len] = nbr % 10 + '0';
		nbr /= 10;
	}
	if (n < 0)
		str[0] = '-';
	return (str);
}
