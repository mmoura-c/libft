/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoura-c <student.42sp.org.br>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/02 14:09:04 by mmoura-c          #+#    #+#             */
/*   Updated: 2021/06/23 06:46:33 by mmoura-c         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const unsigned char	*s_;

	s_ = (const unsigned char *)s;
	while (n-- > 0)
	{
		if (*s_ == (unsigned char)c)
			return ((void *)s_);
		s_++;
	}
	return (0);
}
